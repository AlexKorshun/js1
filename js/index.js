"use strict";

let valid = false;
let name_in = "";
let age_in = 0;
do {
    valid = true;
    let name = prompt("What is your name?", name_in);
    let age = prompt("How old are you?", age_in);

    if (name === '') {
        alert("Your name is empty!");
        valid = false;
    }
    if (age === '' || (Number.isNaN(Number.parseInt(age)))) {
        alert("Your age is not a number!");
        valid = false;
    }
    name_in = name;
    age_in = Number.parseInt(age);
} while (!valid)

if (age_in < 18) {
    alert('You are not allowed to visit this website');
} else if (age_in >= 18 && age_in <= 22) {
    let agree = confirm("Are you sure you want to continue?");
    if (agree === true) {
        alert(`Welcome, ${name_in}`);
    } else {
        alert('You are not allowed to visit this website');
    }
} else {
    alert(`Welcome, ${name_in}`);
}


